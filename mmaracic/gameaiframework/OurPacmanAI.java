/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mmaracic.gameaiframework;

import com.jme3.math.Vector3f;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * @author Marijo
 */
public class OurPacmanAI extends AgentAI{

    protected enum Target {
        POINT, POWERUP, GHOST, EMPTY, START, ESCAPING
    }

    protected static class Location implements Comparable<Location>
    {
        int x,y;
        
        Location(int x, int y)
        {this.x=x; this.y=y;}
        
        int getX() {return x;}
        int getY() {return y;}
        
        @Override
        public boolean equals(Object o)
        {
            if (o instanceof Location)
            {
                Location temp = (Location) o;
                return (temp.x == this.x) && (temp.y == this.y);
            }
            else
                return false;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 79 * hash + this.x;
            hash = 79 * hash + this.y;
            return hash;
        }
        
        public double distanceTo(Location other)
        {
            if(other == null) {
                return Double.MAX_VALUE;
            }

            int distanceX = other.x - x;
            int distanceY = other.y - y;
            
            return Math.abs(distanceX) + Math.abs(distanceY);
//            return Math.sqrt(distanceX*distanceX + distanceY+distanceY);
        }
        
        @Override
        public int compareTo(Location o) {
            if (x==o.x)
            {
                return Integer.compare(y, o.y);
            }
            else
            {
                return Integer.compare(x, o.x);
            }
        }

        @Override
        public String toString() {
            return "x: " + this.x + ", y: " + this.y;
        }
    }

    private static final int GHOST_FEAR_RADIUS = 2;
    private static final double EPSILON = 1E-6;

    private final HashSet<Location> allTraversibleSpots = new HashSet<>();
    private final HashSet<Location> allPoints = new HashSet<>();
    private final HashSet<Location> allPowerUps = new HashSet<>();
    private final HashSet<Location> allExploredSpots = new HashSet<>();

    private final HashSet<Location> pointsInRadius = new HashSet<>();
    private final HashSet<Location> powerUpsInRadius = new HashSet<>();
    private final HashSet<Location> ghostsInRadius = new HashSet<>();
    private final HashMap<Location, Double> criticalGhosts = new HashMap<>();
    private Location myLocation = new Location(0, 0);
    
    private final Date now = new Date();
    private final Random r = new Random(now.getTime());
    
    private Location targetLocation = myLocation;
    private double targetDistance = Double.MAX_VALUE;
    private int targetDuration = 0;
    private Target targetType = Target.START;

    private boolean consecutiveExploration = false;
    private boolean currentlyPoweredUp = false;

    private int selectMoveIndexRandomExploration(ArrayList<int[]> moves) {
        int tmpIndex = r.nextInt(moves.size());
        int[] move = moves.get(tmpIndex);
        myLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);

        // Need to keep this updated, just in case!
        targetLocation = myLocation;
        targetDuration = 0;

        return tmpIndex;
    }

    private int metaExploration(ArrayList<int[]> moves, Location nearestGhostLocation, double nearestGhostDistance) {
        /*
        First pacman wants to check out whether he remembers any power ups or points left around, and go in that
        direction.

        If not true, he wants to see which areas he hasn't explored yet
         */

        printStatus("metaExploration!");

        if(!this.consecutiveExploration) {
            this.consecutiveExploration = true;

            if(!this.allPowerUps.isEmpty()) {
                // Find the nearest Power Up, and go towards it
                Location nearestPowerUpLocation = myLocation;
                double nearestPowerUpDistance = Double.MAX_VALUE;
                double newPowerUpDistance;

                for(Location currPowerUp : this.allPowerUps) {
                    newPowerUpDistance = myLocation.distanceTo(currPowerUp);

                    if(newPowerUpDistance < nearestPowerUpDistance) {
                        nearestPowerUpLocation = currPowerUp;
                        nearestPowerUpDistance = newPowerUpDistance;
                    }
                }

                targetLocation = nearestPowerUpLocation;
                targetDistance = nearestPowerUpDistance;
                targetType = Target.POWERUP;
                targetDuration = 0;
            } else if(!this.allPoints.isEmpty()) {
                // Find the nearest Point, and go towards it
                Location nearestPointLocation = myLocation;
                double nearestPointDistance = Double.MAX_VALUE;
                double newPointDistance;
                for(Location currPoint : this.allPoints) {
                    newPointDistance = myLocation.distanceTo(currPoint);

                    if(newPointDistance < nearestPointDistance) {
                        nearestPointLocation = currPoint;
                        nearestPointDistance = newPointDistance;
                    }
                }

                targetLocation = nearestPointLocation;
                targetDistance = nearestPointDistance;
                targetType = Target.POINT;
                targetDuration = 0;
            } else {
                // Will have to decide where to go next
                // Sort them by distance according to my position - then take top 10%
                List<Location> distanceSortedExploredSpots = this.allExploredSpots.parallelStream()
                        .sorted((o1, o2) -> {
                            double dist1 = myLocation.distanceTo(o1);
                            double dist2 = myLocation.distanceTo(o2);

                            if(Math.abs(dist1 - dist2) <= OurPacmanAI.EPSILON) {
                                return 0;
                            } else if(dist1 < dist2) {
                                return 1;
                            } else {
                                return -1;
                            }
                        })
                        .collect(Collectors.toList());

                // Now let us pick bottom 20%(largest elements) of this array, and choose a random target there
                int n = (int)(distanceSortedExploredSpots.size() * 0.2);
                int randomIndex = r.nextInt(n);
                targetLocation = distanceSortedExploredSpots.get(randomIndex);
                targetType = Target.EMPTY;
                targetDistance = myLocation.distanceTo(targetLocation);
                targetDuration = 0;
            }
        }

        // Pick the next move FINALLY - take care to minimise ghost distance while exploring?
        if(!this.currentlyPoweredUp) {
            return this.selectMoveIndexExplorationNormal(moves, nearestGhostLocation, nearestGhostDistance);
        } else {
            return this.selectMoveIndexExplorationPowerUp(moves);
        }
    }

    private int selectMoveIndexPanicAdvanced(ArrayList<int[]> moves,
                                     Location nearestGhostLocation, double nearestGhostDistance
                                     ) {
        /*
        Avoid the ghost at all costs, go where the distance to him would be maximised
         */
        // TODO: maybe finish this later?
        Location bestLocation = this.myLocation;
        for(int i=moves.size()-1; i>=0; i--) {
            // Evaluate every move against every ghost from criticalGhosts
            int[] move = moves.get(i);
            Location moveLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);

            for(Map.Entry<Location, Double> criticalGhost : this.criticalGhosts.entrySet()) {

            }
        }
        
        return 0;
    }

    private int selectMoveIndexPanic(ArrayList<int[]> moves, Location nearestGhostLocation, double nearestGhostDistance) {
        /*
        Maximise distance to nearest Ghost. If more candidate moves apply, pick the one that is closer to
        a point/PowerUp.
         */
        // Select move
        double currLocMinPDistance = Double.MAX_VALUE;
        double currLocMaxGDistance = Double.MIN_VALUE;
        Location nextLocation = myLocation;
        int moveIndex = 0;

        for (int i=moves.size()-1; i>=0; i--)
        {
            int[] move = moves.get(i);
            Location moveLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);

            double newPDistance = moveLocation.distanceTo(targetLocation);
            double newGDistance = moveLocation.distanceTo(nearestGhostLocation);

            if (newGDistance >= (currLocMaxGDistance - OurPacmanAI.EPSILON))
            {
                // Check if new ghost distance is equal, or actually better than
                if(Math.abs(newGDistance - currLocMaxGDistance) <= OurPacmanAI.EPSILON) {
                    // They are (about) equal - compare them by Point distances instead!
                    if(newPDistance < (currLocMinPDistance - EPSILON)) {
                        // Okay, this one's better as well
                        currLocMinPDistance = newPDistance;
                        currLocMaxGDistance = newGDistance;

                        nextLocation = moveLocation;
                        moveIndex = i;
                    } else {
                        // They're literally of the same quality - for now meh, but maybe could use this later
                    }
                } else {
                    // This one's truly better
                    currLocMinPDistance = newPDistance;
                    currLocMaxGDistance = newGDistance;

                    nextLocation = moveLocation;
                    moveIndex = i;
                }
            }
        }

        myLocation = nextLocation;
        return moveIndex;
    }

    private int selectMoveIndexNormal(ArrayList<int[]> moves, Location nearestGhostLocation, double nearestGhostDistance) {
        // Select move
        double currMinPDistance = Double.MAX_VALUE;
        Location nextLocation = myLocation;
        int moveIndex = 0;

        for (int i=moves.size()-1; i>=0; i--)
        {
            int[] move = moves.get(i);
            Location moveLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);
            double newPDistance = moveLocation.distanceTo(targetLocation);
            double newGDistance=(nearestGhostDistance<Double.MAX_VALUE)?moveLocation.distanceTo(nearestGhostLocation):Double.MAX_VALUE;
            if (newPDistance<=currMinPDistance && newGDistance>GHOST_FEAR_RADIUS)
            {
                //that way
                currMinPDistance = newPDistance;
                nextLocation = moveLocation;
                moveIndex = i;
            }
        }

        myLocation = nextLocation;
        return moveIndex;
    }

    private int selectMoveIndexPowerUp(ArrayList<int []> moves) {
        // Now we wanna move towards the target - try to optimise to get as many Points on the way as possible
        // TODO: Optimise this as to maximise number of points picked up on the way
        // Now select the next move
        double currMinDistance = Double.MAX_VALUE;
        Location nextLocation = myLocation;
        int moveIndex = 0;
        for (int i=moves.size()-1; i>=0; i--)
        {
            int[] move = moves.get(i);
            Location moveLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);

            double newDistance = moveLocation.distanceTo(targetLocation);
            if (newDistance <= currMinDistance)
            {
                currMinDistance = newDistance;
                nextLocation = moveLocation;
                moveIndex = i;
            }
        }

        this.myLocation = nextLocation;
        return moveIndex;
    }

    private int selectMoveIndexExplorationPowerUp(ArrayList<int[]> moves) {
        return this.selectMoveIndexPowerUp(moves);
    }

    private int selectMoveIndexExplorationNormal(ArrayList<int[]> moves, Location nearestGhostLocation,
                                                 double nearestGhostDistance) {
        return this.selectMoveIndexNormal(moves, nearestGhostLocation, nearestGhostDistance);
    }

    @Override
    public int decideMove(ArrayList<int []> moves, PacmanVisibleWorld mySurroundings, WorldEntity.WorldEntityInfo myInfo)
    {
        int radiusX = mySurroundings.getDimensionX()/2;
        int radiusY = mySurroundings.getDimensionY()/2;
               
        boolean powerUp = myInfo.hasProperty(PacmanAgent.powerupPropertyName);
        Vector3f pos = myInfo.getPosition();
//        printStatus("Pacman current pos: " + pos);
//        printStatus("Pacman current Location x: " + pos.x + " y: " + pos.y);

        double nearestGhostDistance = Double.MAX_VALUE;
        Location nearestGhostLocation = null;
        double nearestPointDistance = Double.MAX_VALUE;
        Location nearestPointLocation = null;
        double nearestPowerUpDistance = Double.MAX_VALUE;
        Location nearestPowerUpLocation = null;
        boolean panicMode = false;

        pointsInRadius.clear();
        powerUpsInRadius.clear();
        ghostsInRadius.clear();
        criticalGhosts.clear();

        if(myLocation == targetLocation) {
            switch(this.targetType) {
                case GHOST, EMPTY, START, ESCAPING -> {
                    // For now nothing
                }
                case POINT -> {
                    // Remove its Location from the allPoints
                    this.allPoints.remove(this.targetLocation);
                }
                case POWERUP -> {
                    // Remove its Location from the allPoints
                    this.allPowerUps.remove(this.targetLocation);
                }
                default -> printStatus("Unknown Target type: " + this.targetType.name());
            }
        } else {
            targetDuration++;
        }

        for (int i = -radiusX; i <= radiusX; i++)
        {
            for (int j = -radiusY; j <= radiusY; j++)
            {
                if (i==0 && j==0) continue;

                Location tempLocation = new Location(myLocation.getX()+i, myLocation.getY()+j);
                ArrayList<WorldEntity.WorldEntityInfo> neighPosInfos = mySurroundings.getWorldInfoAt(i, j);
                if (neighPosInfos != null)
                {
                    this.allExploredSpots.add(tempLocation);

                    for (WorldEntity.WorldEntityInfo info : neighPosInfos) {
                        if (info.getIdentifier().compareToIgnoreCase("Pacman") == 0) {
                            // Ignore myself
                        } else if (info.getIdentifier().compareToIgnoreCase("Wall") == 0) {
                            // Its a wall, who cares!
                        } else if (info.getIdentifier().compareToIgnoreCase("Point") == 0) {
                            // Remember where it is!
//                            printStatus("Point position -> " + tempLocation.toString());
                            pointsInRadius.add(tempLocation);
                            double currPointDistance = myLocation.distanceTo(tempLocation);
                            if(currPointDistance < nearestPointDistance) {
                                nearestPointDistance = currPointDistance;
                                nearestPointLocation = tempLocation;
                            }
                        } else if (info.getIdentifier().compareToIgnoreCase("PowerUp") == 0) {
                            // Remember PowerUp location
//                            printStatus("Point position -> " + tempLocation.toString());
                            powerUpsInRadius.add(tempLocation);
                            double currPowerUpDistance = myLocation.distanceTo(tempLocation);
                            if(currPowerUpDistance < nearestPowerUpDistance) {
                                nearestPowerUpDistance = currPowerUpDistance;
                                nearestPowerUpLocation = tempLocation;
                            }
                        }
                        else if (info.getIdentifier().compareToIgnoreCase("Ghost")==0)
                        {
                            // Remember him!
                            ghostsInRadius.add(tempLocation);
                            double currGhostDistance = myLocation.distanceTo(tempLocation);
                            if (currGhostDistance < nearestGhostDistance)
                            {
                                nearestGhostDistance = currGhostDistance;
                                nearestGhostLocation = tempLocation;
                            }

                            if(currGhostDistance <= OurPacmanAI.GHOST_FEAR_RADIUS) {
                                panicMode = true;
                                this.criticalGhosts.put(tempLocation, currGhostDistance);
                            }
                        }
                        else
                        {
                            printStatus("I dont know what " + info.getIdentifier() + " is!");
                        }
                    }
                }
            }            
        }

        /*
        Now we have got all guidance data and metadata ready - let's decide what to do!
        The algorithm goes as follows:

            Prleft = 20%
            Gr = 2 (GHOST_FEAR_RADIUS class constant)

        First see if pacman is Powered-Up:
            a) Powered-up and more than Prleft% duration left
                - Primarily chase ghosts
                - Secondarily chase other power ups
                - Lastly go after points

            d) Post-death mode - after dying pacman behaves just like not Powered-Up
            b) Not powered-up or Powered-Up but near the end of the power-up effect(after a certain threshold):
                - Chase primarily power ups
                - Secondarily chase points
                - Avoid ghosts if they are in radius of Gr

            c) Panic mode - if not powered up and ghosts in critical radius then just escape from them

            Pacman first attempts to gain all the power ups, eating all the points by the way, and avoiding ghosts if
            necessary

            e) Exploration mode
            If pacman doesn't know where to go - he goes to areas not explored before where the above algorithm is again
            applied!

            So, pacman has got nothing of interest in his radius, therefore he needs to explore other areas
                - Check if pacman has got any other not-in-radius power ups remembered - and go after them
                - Check if pacman has got any other not-in-radius points remembered - and go after them
                - If neither of the above points applies, then Pacman can:
                    - Go random(mby implement this first?)
                    - Go to the closest area not yet explored
         */

        boolean need2Explore = false;
        // Am I powered up?
        if(powerUp && myInfo.getProperty(PacmanAgent.powerupPropertyName).equalsIgnoreCase("True")) {
            this.currentlyPoweredUp = true;

            // Under power up - first check if any ghosts are present and chase them
            if(nearestGhostLocation != null) {
                // Go after the ghosts
                targetLocation = nearestGhostLocation;
            } else if(!powerUpsInRadius.isEmpty()) {
                // No ghosts, but there is a Power Up, so go after it
                targetLocation = nearestPowerUpLocation;
            } else if(!pointsInRadius.isEmpty()) {
                // No Ghosts or PowerUps, but we can go after the closest Point I guess!
                targetLocation = nearestPointLocation;
            } else {
                // Nothing in our radius - so we need to go somewhere new
                need2Explore = true;

                // TODO: first random, then implement something more meaningful
                return this.metaExploration(moves, nearestGhostLocation, nearestGhostDistance);
            }
            this.consecutiveExploration = false;

            targetDistance = myLocation.distanceTo(targetLocation);
            targetDuration = 0;

            return this.selectMoveIndexPowerUp(moves);
        } else {
            this.currentlyPoweredUp = false;

            if(panicMode) {
                targetLocation = null;
                targetDistance = Double.MAX_VALUE;
                targetType = Target.ESCAPING;
                targetDuration = 0;

                return this.selectMoveIndexPanic(moves, nearestGhostLocation, nearestGhostDistance);
            }

            // Not powered up - normal mode or post-death
            if(!powerUpsInRadius.isEmpty()) {
                // Let's chase Power Ups first
                targetLocation = nearestPowerUpLocation;
            } else if(!pointsInRadius.isEmpty()) {
                // No Power Ups, so let us chase Points instead
                targetLocation = nearestPointLocation;
            } else {
                // Nothing in our radius - so just go random!
                need2Explore = true;

                // TODO: first random, then implement something more meaningful
                return this.metaExploration(moves, nearestGhostLocation, nearestGhostDistance);
            }
            this.consecutiveExploration = false;

            targetDistance = myLocation.distanceTo(targetLocation);
            targetDuration = 0;

            return this.selectMoveIndexNormal(moves, nearestGhostLocation, nearestGhostDistance);
        }

//        // Move toward the point
//        // Pick next if arrived
//        if (targetLocation==myLocation)
//        {
//            targetLocation = pointsInRadius.iterator().next();
//            targetDistance = myLocation.distanceTo(targetLocation);
//            targetDuration=0;
//        }
//
//         targetDuration++;
//
//        // Sticking with target too long -> got stuck
//        // Dont get stuck
//       if (targetDuration>10)
//        {
//            ArrayList<Location> pointList = new ArrayList<>(pointsInRadius);
//            int choice = r.nextInt(pointList.size());
//
//            targetLocation = pointList.get(choice);
//            targetDistance = myLocation.distanceTo(targetLocation);
//            targetDuration = 0;
//        }
//
//        // Select move
//        double currMinPDistance = Double.MAX_VALUE;
//        Location nextLocation = myLocation;
//        int moveIndex = 0;
//
//        for (int i=moves.size()-1; i>=0; i--)
//        {
//            int[] move = moves.get(i);
//            Location moveLocation = new Location(myLocation.getX()+move[0], myLocation.getY()+move[1]);
//            double newPDistance = moveLocation.distanceTo(targetLocation);
//            double newGDistance=(nearestGhostDistance<Double.MAX_VALUE)?moveLocation.distanceTo(nearestGhostLocation):Double.MAX_VALUE;
//            if (newPDistance<=currMinPDistance && newGDistance>1)
//            {
//                //that way
//                currMinPDistance = newPDistance;
//                nextLocation = moveLocation;
//                moveIndex = i;
//            }
//       }
//
//        myLocation = nextLocation;
//        return moveIndex;
    }
}
